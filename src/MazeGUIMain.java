/**
 * Spring 2018
 */


import java.awt.EventQueue;

/**
 * Starting point for the GUI interface. 
 * 
 * @author davidshannon
 * @version 1
 */
public final class MazeGUIMain {
    

    /** Private Constructor to prohibit class instantiation. */
    private MazeGUIMain() { }

    /**
     * Starting point for program.
     * 
     * @param theArgs Command line arguments.  
     */
    public static void main(final String[] theArgs) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MazeGUI().start();                                 
            }
        });

    }
}

