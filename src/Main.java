/**
 * Spring 2018
 * @author davidshannon
 * @version 1
 *
 */


public class Main {

	public static void main(String[] args) {
		Maze mazeDebug = new Maze(5, 5, true);
		mazeDebug.display();
		
		Maze otherMaze = new Maze(12, 18, false);
		otherMaze.display();
	}

}
