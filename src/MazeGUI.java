/**
 * Sprint 2018
 */

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class MazeGUI {

	/** The size in pixels of a side of one "square" on the grid. */
	private static final int MY_SQUARE_SIZE = 10;

	/** JFrame to display maze. */
	private JFrame myJFrame;

	/** GUI panel to display maze. */
	private MazeGUIPanel myMazeGUIPanel;

	public MazeGUI() {
		myJFrame = new JFrame("Maze Generation");
		myMazeGUIPanel = new MazeGUIPanel(MY_SQUARE_SIZE);
	}

	public void start() {
		myMazeGUIPanel.repaint();
		myJFrame.add(myMazeGUIPanel, BorderLayout.CENTER);
		myJFrame.pack();
		myJFrame.setResizable(false);
		myJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myJFrame.setVisible(true);

	}

}
