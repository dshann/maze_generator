import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * Spring 2018
 * @author davidshannon
 * @verision 1
 *
 */



public class MazeGUIPanel extends JPanel implements ActionListener {
	
	/** Generated Serial Version ID. */
	private static final long serialVersionUID = 5445210371945217094L;
	
	/** Used for number of rows */
	private static final int N = 30;
	
	/** Used for number of columns . */
	private static final int M = 40;
	
	/** Used to set delay for timer updating GUI. */
	private static final int TIMER_DELAY = 0;

	/** String representation of the maze to be displayed. */
	private String myMazeString;
	
    /** Array of each line from myStringBoard. */
    private String[] myStrings;

	/** Size, in pixels, of each node/wall. */
	private int mySquareSize;
	
	/** The maze to be displayed. */
	private Maze myMaze;
	
	/** Array list of mazes to be drawn on GUI. */
	private ArrayList<String> myDrawnMazes;
	
	/** Timer used for displaying each step in the maze. */
	private Timer myTimer;
	

	
	
	public MazeGUIPanel(final int theSqSize) {
		super();
		mySquareSize = theSqSize;
		myMaze = new Maze(N, M, true);
		myMaze.display();
		this.setPreferredSize(new Dimension(theSqSize * (N * 2 + 1), theSqSize * (M * 2 + 1)));
		myDrawnMazes = myMaze.myDrawnMazesStrings;
		myTimer = new Timer(TIMER_DELAY, this); 
		myTimer.start();
	}
	
	   /**
     * Paints the game board. Triggered by repaint() method call.
     * 
     * @param theGraphics The graphics context to use for painting.
     */
    @Override
    public void paintComponent(final Graphics theGraphics) {
        super.paintComponent(theGraphics);
        final Graphics2D g2d = (Graphics2D) theGraphics;

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                             RenderingHints.VALUE_ANTIALIAS_ON);
        
        if (myDrawnMazes.size() == 1) {
        	myMazeString = myDrawnMazes.get(0);
        	myTimer.stop();
        } else {
        	myMazeString = myDrawnMazes.get(0);
        	myDrawnMazes.remove(0);
        }

        myStrings = myMazeString.split("\n");

        for (int x = 0; x < myMaze.myDepth * 2 + 1; x++) {
            for (int y = 0; y < myMaze.myWidth * 2 + 1; y++) {
                final Rectangle2D.Double block =
						new Rectangle2D.Double((double) (y) * mySquareSize, (double) (x) * mySquareSize,
								(double) mySquareSize, (double) mySquareSize);
				if (myStrings[x].charAt(y) == ' ') {
					g2d.setPaint(Color.WHITE);
					g2d.draw(block);
				} else if (myStrings[x].charAt(y) == 'X') {
					g2d.setPaint(Color.BLACK);
					g2d.fill(block);
				} else if (myStrings[x].charAt(y) == 'V') {
					g2d.setPaint(Color.WHITE);
					g2d.fill(block);
				} else if (myStrings[x].charAt(y) == '+') {
					g2d.setPaint(Color.GREEN);
					g2d.fill(block);
				}
			}
		}
	}

	@Override
	public void actionPerformed(final ActionEvent theEvent) {
		if (theEvent.getSource().equals(myTimer)) {
			repaint();
		}
		
	}

}
	

