/**
 * Spring 2018
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;



/**
 * Class for instantiating Maze Generator. 
 * @author davidshannon
 * @version 1
 */
public class Maze {
	
	/** Random generator used to assign values to edges of maze. */
	public static final Random MY_RANDOM = new Random();
	
	/** Width of maze, n. */
	protected int myWidth;
	
	/** Depth of maze, m. */
	protected int myDepth;
	
	/** Shows steps of maze creation when set to true. */
	protected boolean myDebug;
	
	/** Array for representing layout of Graph. */
	private Node[][] G;
	
	/** Wall list used for creating maze using Prim's algorithm. */
	private List<Wall> myTempWallList;
	
	/** List of visited nodes during breadth-first search. */
	private List<Node> myVisitedList;
	
	/** Queue for performing breadth-first search. */
	private Queue<Node> myQueue;
	
	/** Map of shortest path. */
	private Map<Node, Node> myTempPathPairs;
	
	/** List of nodes in solution. */
	private List<Node> mySolutionList;
	
	/** String output for maze template. */
	private String myMazeTemplateString;
	
	/** String output for unsolved maze. */
	private String myUnsolvedMazeString;
	
	/** String output for solved maze. */
	private String mySolvedMazeString;
	
	/** ArrayList for all drawn mazes. Used for GUI. */
	protected ArrayList<String> myDrawnMazesStrings;
	
	

	
	/** 
	 * Constructor for Maze Object.
	 * 
	 * @param width n
	 * @param depth m
	 * @param debug shows results or not. 
	 */
	Maze(int width, int depth, boolean debug) {
		myWidth = width;
		myDepth = depth;
		myDebug = debug;
		G = new Node[myDepth][myWidth];
		myDrawnMazesStrings = new ArrayList<String>();
	}
	
	/**
	 * Method for displaying the maze using Xs and ' ' chars;
	 * '+' characters marks the correct path. 
	 */
	public void display() {
		setUpMazeTemplate();
		myDrawnMazesStrings.add(myMazeTemplateString); //used for GUI
		System.out.println("\nmywidth: " + myWidth + "  mydepth: " + myDepth + "\n");
		generateSpanningTree(G[MY_RANDOM.nextInt(myDepth)][MY_RANDOM.nextInt(myWidth)]);
		if (!myDebug) {
			buildUnsolvedMaze();
			System.out.print(myMazeTemplateString + "\nMy Generated Maze: \n\n" + myUnsolvedMazeString + "\n\n");
		}
		findSolutionPath(G[0][0], G[myDepth-1][myWidth-1]);
		getSolution();
		myDrawnMazesStrings.add(mySolvedMazeString);
		System.out.print("My Solved Maze: \n\n" + mySolvedMazeString + "\n\n");
	}
	
	/**
	 * Private helper method for adding Nodes to the initial Graph Array.
	 */
	private void setUpMazeTemplate() {
		
		
		//sets up all the nodes with walls and adds corresponding nodes to each wall
		for (int i = 0; i < myDepth; i++) {
			for (int j = 0; j < myWidth; j++) {
				Node node = new Node(i, j);
				G[i][j] = node;
				
				// assigns NORTH walls:
				if (i == 0) {
					node.myNodeWalls[0] = new Wall();
				} else {
					Wall w0 = G[i - 1][j].myNodeWalls[1];
					node.myNodeWalls[0] = w0;
					//if (!w0.myNodeList.contains(node)) {
						w0.myNodeList.add(node);
					//}
				}
				
				//assigns SOUTH walls:
				Wall w1 = new Wall();
				node.myNodeWalls[1] = w1;
				w1.myNodeList.add(node);
				//add north wall
				
				
				//assigns EAST walls:
				Wall w2 = new Wall();
				node.myNodeWalls[2] = w2;
				w2.myNodeList.add(node);
				//add west wall
				
				//assign WEST walls:
				if (j == 0) {
					node.myNodeWalls[3] = new Wall();
				} else {
					Wall w3 = G[i][j-1].myNodeWalls[2];
					node.myNodeWalls[3] = w3;
					w3.myNodeList.add(node);
				}
				
				//assigns BORDER value to n/s walls
				if (i == 0) {
					node.myNodeWalls[0].myBorder = true;		
				} else if (i == myDepth - 1) {
					node.myNodeWalls[1].myBorder = true;
				}
				//assigns BORDER value to east/west walls
				if (j == 0) {
					node.myNodeWalls[3].myBorder = true;
				} else if (j == myWidth - 1) {
					node.myNodeWalls[2].myBorder = true;
				}
			}
		}
		
		
		////////////////////
		///BUILDING MAZE////
		////////////////////
		
		StringBuilder sb = new StringBuilder();
		
		// print initial top border
		for (int col = 0; col < myWidth * 2 + 1; col++) {
			if (col == 1) {
				sb.append(" ");
			} else {
				sb.append("X");
			}
		}
		sb.append("\n");

		// prints each row, alternating between cell and wall
		for (int y = 0; y < myDepth; y++) {
			sb.append("X");
			for (int x = 0; x < myWidth; x++) {
				//selects if wall or cell row:
				if (!G[y][x].myNodeWalls[2].myBorder) {
					if(!G[y][x].myNodeWalls[2].myPassable) { 
						sb.append(" X");
					} else {
						sb.append("  ");
					}
				} 
			}
			// print right edge
			sb.append(" X\n");
			
			// print next row of walls
			for (int col = 0; col < myWidth * 2 + 1; col++) {
				if (y == myDepth - 1 && col == myWidth * 2 - 1) {
					sb.append(" ");
				} else {
					sb.append("X");
				}
			}
			sb.append("\n");
		}
		
		myMazeTemplateString = sb.toString();
		
	}
	
	
	/**
	 * Private helper Method for carving possible paths in graph array;
	 * Starts from random node in the graph and produces graph from there.
	 */
	private void generateSpanningTree(final Node node) {
		myTempWallList = new ArrayList<Wall>();
		node.myNodePassable = true;
		
		// add walls of the node to the wall list
		for (int i = 0; i < 4; i++) {
			Wall w = node.myNodeWalls[i];
			if (!w.myBorder) {						
				myTempWallList.add(w);
			}	
		}	
		
		
		// implements Prim's algorithm to carve out maze
		while (!myTempWallList.isEmpty()) {
			if(myDebug) {
				buildUnsolvedMaze();
				System.out.println(myUnsolvedMazeString + "\n\n");
			}
			Wall w = myTempWallList.get(MY_RANDOM.nextInt((myTempWallList.size())));	//pick random wall from list
			Node n1 = w.myNodeList.get(0);
			Node n2 = w.myNodeList.get(1);
			if (!(n1.myNodePassable && n2.myNodePassable)) { //if only one of 2 nodes that divides it is visited
				w.myPassable = true;			//mark the wall as passable
				
				if (!n1.myNodePassable) {
					n1.myNodePassable = true;
					for (int i = 0; i < 4; i++) {
						Wall w2 = n1.myNodeWalls[i];
						if (!w2.myBorder && !myTempWallList.contains(w2)) {
							myTempWallList.add(w2);
						}	
					}
				} else if (!n2.myNodePassable) {
					n2.myNodePassable = true;
					for (int i = 0; i < 4; i++) {
						Wall w2 = n2.myNodeWalls[i];
						if (!w2.myBorder && !myTempWallList.contains(w2)) {
							myTempWallList.add(w2);
						}	
					}
				}
			}	
			myTempWallList.remove(w);
		}	
	}
		

        ////////////////////////////////
		////////////////////////////////
		/////BUILDING UNSOLVED MAZE/////
		////////////////////////////////
        ////////////////////////////////
		
	public void buildUnsolvedMaze() {
			StringBuilder s = new StringBuilder();
			
			//print initial top border
			for (int col = 0; col < myWidth * 2 + 1; col++) {
				if (col == 1) {
					s.append(" ");
				} else {
					s.append("X");
				}
			}
			s.append("\n");
			
			//prints each row, alternating between nodes and walls
			for (int y = 0; y < myDepth; y++) {
				for (int cnt = 0; cnt < 2; cnt++) {
					s.append("X");
					for (int x = 0; x < myWidth; x++) {
						if (cnt == 0) {
							if (!G[y][x].myNodeWalls[3].myBorder) {			//if WEST WALL is not border,
								if (!G[y][x].myNodeWalls[3].myPassable) { 	//if WEST WALL not passable,
									s.append("X ");								
								} else {
									s.append("V ");
								}	
							} else {						//if WEST WALL is border
								s.append(" ");
							}
						} else if (cnt == 1) {
							try {
								if (!G[y+1][x].myNodeWalls[0].myBorder && x != myWidth - 1) {	//if NORTH WALL is not border
									if(!G[y+1][x].myNodeWalls[0].myPassable) { 		//if NORTH WALL is not passable
										s.append("XX");						//this is wrong				
									} else {
										s.append("VX");				//this is wrong
									}
								}  else {
									if (!G[y+1][x].myNodeWalls[0].myPassable) {
										s.append("X");
									} else {
										s.append("V");
									}
								}
							} catch (ArrayIndexOutOfBoundsException e) {
							}
						}
					}
					if (cnt == 0 ) {
						s.append("X\n");
					}
				}
				
				if (y != myDepth - 1) {
					s.append("X\n");
				}
			}
			//print final border row of walls
			for (int col = 0; col < myWidth * 2; col++) {
				if (col == myWidth * 2 - 2) {
					s.append(" ");
				} else {
					s.append("X");
				}
			}
					
			
		myUnsolvedMazeString = s.toString();
		myDrawnMazesStrings.add(myUnsolvedMazeString);
			
	}
	
	
	/** 
	 * Method for implementing breadth-first search to find solution of shortest path; 
	 * Searches N, S, E, W order.
	 */
	private void findSolutionPath(final Node startNode, final Node endNode) {
		myVisitedList = new ArrayList<Node>();
		myTempPathPairs = new HashMap<Node, Node>();
		mySolutionList = new ArrayList<Node>();
		myQueue = new LinkedList<Node>();
		myQueue.add(startNode);
		myVisitedList.add(startNode);
		Node current = startNode;
		while(!myQueue.isEmpty()) {
			current = myQueue.remove();
			
			if (!current.equals(endNode)) {
				//north
				if (!current.myNodeWalls[0].myBorder && current.myNodeWalls[0].myPassable 			//north wall is not border, is passable
						&& !myVisitedList.contains(G[current.myY - 1][current.myX])) {	//and the node above has not yet been visited
					myQueue.add(G[current.myY-1][current.myX]);		//adds NORTH NODE to queue
					myVisitedList.add(G[current.myY-1][current.myX]);
					myTempPathPairs.put(G[current.myY-1][current.myX], current);
					//System.out.println("n");
				}
				//south
				if (!current.myNodeWalls[1].myBorder && current.myNodeWalls[1].myPassable 			//south wall is not border, is passable
						&& !myVisitedList.contains(G[current.myY + 1][current.myX])) {	//and the node below has not yet been visited
					myQueue.add(G[current.myY + 1][current.myX]);		//adds SOUTH NODE to queue
					myVisitedList.add(G[current.myY + 1][current.myX]);
					myTempPathPairs.put(G[current.myY + 1][current.myX], current);
					//System.out.println("s");
				}
				//east
				if (!current.myNodeWalls[2].myBorder && current.myNodeWalls[2].myPassable 			//east wall is not border, is passable
						&& !myVisitedList.contains(G[current.myY][current.myX + 1])) {	//and the node to right has not yet been visited
					myQueue.add(G[current.myY][current.myX + 1]);		//adds EAST NODE to queue
					myVisitedList.add(G[current.myY][current.myX + 1]);
					myTempPathPairs.put(G[current.myY][current.myX + 1], current);
					//System.out.println("e");
				}
				//west
				if (!current.myNodeWalls[3].myBorder && current.myNodeWalls[3].myPassable 			//west wall is not border, is passable
						&& !myVisitedList.contains(G[current.myY][current.myX - 1])) {	//and the node to left has not yet been visited
					myQueue.add(G[current.myY][current.myX - 1]);		//adds WEST NODE to queue
					myVisitedList.add(G[current.myY][current.myX - 1]);
					myTempPathPairs.put(G[current.myY][current.myX - 1], current);
					//System.out.println("w");
				}
			
			} else {
				Node node = endNode;
				while(node != startNode) {
					mySolutionList.add(node);
					node = myTempPathPairs.get(node);
				}
				mySolutionList.add(G[0][0]);
				Collections.reverse(mySolutionList);
				//System.out.print("\n\n" + mySolutionList);
				break;
			}
			
		}
	} 
	

	/**
	 * Method for generating solved maze string. 
	 */
	private void getSolution() {
		 
		////////////////////////////////
		////////////////////////////////
		///// BUILDING SOLVED MAZE//////
		////////////////////////////////
		////////////////////////////////

		
		StringBuilder s = new StringBuilder();
		
		
		// print initial top border
		for (int col = 0; col < myWidth * 2 + 1; col++) {
			if (col == 1) {
				s.append("+");
			} else {
				s.append("X");
			}
		}
		s.append("\n");

		// prints each row, alternating between nodes and walls
		for (int y = 0; y < myDepth; y++) {
			for (int cnt = 0; cnt < 2; cnt++) {
				s.append("X");
				for (int x = 0; x < myWidth; x++) {
					if (cnt == 0) {
						if (!G[y][x].myNodeWalls[3].myBorder) { // if WEST WALL is not border,
							if (!G[y][x].myNodeWalls[3].myPassable) { // if WEST WALL not passable,
								if (mySolutionList.contains(G[y][x])) {
									s.append("X+");
								} else {
									s.append("X ");
								}
							} else {
								if (mySolutionList.contains(G[y][x]) && mySolutionList.contains(G[y][x-1])) {
									s.append("++");
								} else if (mySolutionList.contains(G[y][x])) {
									s.append(" +");
								} else {
									s.append("  ");
								}
							}
						} else { // if WEST WALL is border
							if (mySolutionList.contains(G[y][x])) {
								s.append("+");
							} else {
								s.append(" ");
							}
						}
					} else if (cnt == 1) {
						try {
							if (!G[y + 1][x].myNodeWalls[0].myBorder && x != myWidth - 1) { // if NORTH WALL is not border
								if (!G[y + 1][x].myNodeWalls[0].myPassable) { // if NORTH WALL is not passable
									s.append("XX"); 
								} else {
									//System.out.print(mySolutionList);
									if (mySolutionList.contains(G[y][x]) && mySolutionList.contains(G[y+1][x])) {
										s.append("+X");
									} else {
										s.append(" X");
									}	
								}
							} else {
								if (!G[y + 1][x].myNodeWalls[0].myPassable) {
									s.append("X");
								} else {
									if (mySolutionList.contains(G[y][x]) && mySolutionList.contains(G[y+1][x])) {
										s.append("+");
									} else {
										s.append(" ");
									}	
								}
							}
						} catch (ArrayIndexOutOfBoundsException e) {
						}
					}
				}
				if (cnt == 0) {
					s.append("X\n");
				}
			}

			if (y != myDepth - 1) {
				s.append("X\n");
			}
		}
		// print final border row of walls
		for (int col = 0; col < myWidth * 2; col++) {
			if (col == myWidth * 2 - 2) {
				s.append("+");
			} else {
				s.append("X");
			}
		}
		
		mySolvedMazeString = s.toString();
			
	 }
	
	/**  inner class Node Class. */
	public class Node {
		
		/** List of walls.  */
		Wall[] myNodeWalls = new Wall[4]; //north, south, east, west == 0, 1, 2, 3

		/** boolean if node is passable or not. */
		boolean myNodePassable;
		
		/** Boolean used to perform Breadth-first search on map. */
		boolean myVisited;
		
		/** X-coordinate for node. */
		int myX;
		
		/** Y-coordinate for node. */
		int myY;
		
		Node (final int theY, final int theX) {
			myNodePassable = false;
			myVisited = false;
			myX = theX;
			myY = theY;
		}
		
		@Override
		public String toString() {
			return "(Y:" + myY + ", X:" + myX + ")";
		}
	}
	
	/** class for representing wall in maze. */
	public class Wall {
		
		/** List of nodes each wall separates */
		ArrayList<Node> myNodeList;
		
		/** data field indicating whether wall is passable or not. */
		boolean myPassable;
		
		/** data field indicating whether wall is border of graph of not. */
		boolean myBorder;
		
		/** Constructor for wall object. */
		Wall() {
			myPassable = false;
			myBorder = false;
			myNodeList = new ArrayList<Node>();
		}
	}
}




